// import {AuthenticationError} from 'apollo-server';
import { ObjectId } from 'promised-mongo';

const { AuthenticationError, ForbiddenError } = require('apollo-server');

const { query } = require('nact');

const resource = 'news_item';

module.exports = {

  Mutation: {
    changeNewsItem: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      args.input.author_id = new ObjectId(ctx.user._id);
      if (args.input.project_id) {
        args.input.project_id = new ObjectId(args.input.project_id);
      }
      if (args.input.circle_id) {
        args.input.circle_id = new ObjectId(args.input.circle_id);
      }

      if (args._id) {
        return await query(collectionItemActor, { type: 'news_item', search: { _id: args._id }, input: args.input }, global.actor_timeout);
      }
      return await query(collectionItemActor, { type: 'news_item', input: args.input }, global.actor_timeout);
    },
  },

  Query: {

    getNewsItem: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      return await query(collectionItemActor, { type: 'news_item', search: { _id: args.id } }, global.actor_timeout);
    },

    getNewsItems: async (obj, args, ctx, info) => {
      const collectionActor = ctx.children.get('collection');

      return await query(collectionActor, { type: 'news_item' }, global.actor_timeout);
    },
  },
  NewsItem: {
    author: async (obj, args, ctx, info) => {
      if (obj.author_id) {
        let user = (await ctx.db.user.find({ _id: new ObjectId(obj.author_id) }))[0];
        user.full_name = `${user.name} ${user.family_name}`;

        return user;
      }
    },
    circle: async (obj, args, ctx, info) => {
      if (obj.author_id) {
        return (await ctx.db.circle.find({ _id: new ObjectId(obj.circle_id) }))[0];
      }
    },
    project: async (obj, args, ctx, info) => {
      if (obj.author_id) {
        return (await ctx.db.project.find({ _id: new ObjectId(obj.project_id) }))[0];
      }
    },
  },
};
